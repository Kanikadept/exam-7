import React from 'react';
import './OrderItem.css';

const OrderItem = props => {
    return (
        <div onClick={(event) => props.addOrder(event, props.orderItemId)} className="order-item">
          <p>{props.orderItemName}</p>
          <p>Price: {props.orderItemPrice} KGS</p>
        </div>
    );
};

export default OrderItem;