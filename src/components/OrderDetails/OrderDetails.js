import React from 'react';
import './OrderDetails.css';


const OrderDetails = props => {

    let content = '';
    if(props.ordersArr.length === 0) {
        content = <div>Order is empty! <br/> Please add some items!</div>
    } else {
        content = props.ordersArr.map(orderDetails => {
            return <div className="order-details-item" key={orderDetails.id}>
                            <span>{orderDetails.name} </span>
                            <span>(x{orderDetails.count})</span>
                            <span> {orderDetails.price} KGS</span>
            <button onClick={(event) => props.removeOrder(event, orderDetails.id)}>X</button>
            </div>;
        })
    }
    let totalPrice = null;
    if(props.ordersArr.length !== 0) {
        totalPrice = 'Total price: ' + props.totalSum();
    }

    return (
        <div className="orderDetails">
            {content}
            <strong>{totalPrice}</strong>
        </div>
    );
};

export default OrderDetails;