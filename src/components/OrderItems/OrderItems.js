import React from 'react';
import './OrderItems.css';
import OrderItem from "../OrderItem/OrderItem";


const OrderItems = props => {
    return (
        <div className="orderItems">

            {props.orderItems.map(orderItem => {
                return <OrderItem addOrder={props.addOrder}
                                  key={orderItem.id}
                                  orderItemId={orderItem.id}
                                  orderItemName={orderItem.name}
                                  orderItemPrice={orderItem.price}/>
            })}
        </div>
    );
};
export default OrderItems;