import React, {useState} from 'react';
import './FastFood.css';
import OrderDetails from "../../components/OrderDetails/OrderDetails";
import OrderItems from "../../components/OrderItems/OrderItems";


const FastFood = () => {

    const ORDER_ITEMS = [
        {id: 1, name: 'Hamburger', price: 80},
        {id: 2, name: 'Coffee', price: 70},
        {id: 3, name: 'Cheeseburger', price: 90},
        {id: 4, name: 'Tea', price: 50},
        {id: 5, name: 'Fries', price: 45},
        {id: 6, name: 'Cola', price: 40},
    ]

    const [orders, setOrders] = useState([
    ]);

    const addOrder = (event, orderId) => {
        event.stopPropagation();
        const index = orders.findIndex(order => order.id === orderId);
        if(index === -1) {
            const index2 = ORDER_ITEMS.findIndex(order => order.id === orderId);
            const newOrder = ORDER_ITEMS[index2];
            newOrder["count"] = 1;
            setOrders([...orders, newOrder]);
        } else {
            const ordersCopy = [...orders];
            ordersCopy[index].count++;
            const index3 = ORDER_ITEMS.findIndex(order => order.id === ordersCopy[index].id);
            ordersCopy[index].price = ORDER_ITEMS[index3].price * ordersCopy[index].count;

            setOrders(ordersCopy);
        }
    }

    const removeOrder = (event, orderId) => {
        event.stopPropagation();
        const index = orders.findIndex(order => order.id === orderId);
        const ordersCopy = [...orders];
        ordersCopy.splice(index, 1);

        setOrders(ordersCopy);
    }

    const getTotalSum = () => {
        const result = orders.reduce((acc, order) => {
            acc += order.price;
            return acc;
        },0);

        return result;
    }

    return (
        <div className="fast-food">
            <OrderDetails ordersArr={orders} removeOrder={removeOrder} totalSum={getTotalSum}/>
            <OrderItems orderItems={ORDER_ITEMS} addOrder={addOrder} removeOrder={removeOrder}/>
        </div>
    );
};

export default FastFood;