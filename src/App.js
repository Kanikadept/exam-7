import './App.css';
import FastFood from "./containers/FastFood/FastFood";

const App = () => (
    <div className="App">
        <FastFood />
    </div>
);

export default App;
